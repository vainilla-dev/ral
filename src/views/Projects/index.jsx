import React, { useRef } from 'react';
import { useHistory } from 'react-router-dom';

import {
  MainSection,
  ContactTitle,
  SectionButtonContact,
  ImgBuilingComponent,
  SectionFormulario,
  Modal,
  ModalContent,
  Row,
} from "./styles";
import { blue } from "../../assets/styles/colors";
import { Footer } from "../../components/Footer";
import { LinkButton } from "../../components/LinkButton";
import { ButtonsContact } from "../../components/ButtonsContact";
import { ContainerImages } from "../../components/ContainerImages/ContainerImages"
import { GlobalBlur } from '../../components/GlobalBlur/GlobalBlur'
import { processButtonData } from "./processButtonData";
import { edificationButtonData } from "./edificationButtonData";
import { infrastructureButtonData } from "./infrastructureButtonData";
import { industrialButtonData } from "./industrialButtonData";
import { NavbarContainer, Logo, NavLink, NavContact, NavLogo } from '../../components/Navbar/styles';
import LogoRal from '../../assets/images/logos/logo.ral.png';
import { Button } from '../../components/Button';

const email = 'promocion@gruporal.com'

export const Projects = () => {
  const [show, setShow] = React.useState(false)
  const [itemSelected, setItemSelected] = React.useState('')
  const [statusProcessButton, setStatusProcessButton] = React.useState(false)
  const [statusEdificationButton, setStatusEdificationessButton] = React.useState(true)
  const [statusInfrastructureButton, setStatusInfrastructureButton] = React.useState(true)
  const [statusIndustrialButton, setStatusIndustrialButton] = React.useState(true)
  const [data, setData] = React.useState(processButtonData())


  const handleModal = (data, value) => {
    setShow(value)
    setItemSelected(data)
  }

  const history = useHistory();
  function handleRedirect(dir = '/') {
    history.push(dir);
  }

  const divRef = useRef(null);

  const handleButton = button => {
    switch (button) {
      case 'processButton':
        setStatusProcessButton(false)
        setStatusEdificationessButton(true)
        setStatusInfrastructureButton(true)
        setStatusIndustrialButton(true)
        setData(processButtonData())
        break;
      case 'edificationButton':
        setStatusProcessButton(true)
        setStatusEdificationessButton(false)
        setStatusInfrastructureButton(true)
        setStatusIndustrialButton(true)
        setData(edificationButtonData())
        break;
      case 'infrastructureButton':
        setStatusProcessButton(true)
        setStatusEdificationessButton(true)
        setStatusInfrastructureButton(false)
        setStatusIndustrialButton(true)
        setData(infrastructureButtonData())
        break;
      case 'industrialButton':
        setStatusProcessButton(true)
        setStatusEdificationessButton(true)
        setStatusInfrastructureButton(true)
        setStatusIndustrialButton(false)
        setData(industrialButtonData())
        break;
      default:
        break;
    }
  }

  return (
    <>
      <NavbarContainer>
        <NavLogo>
          <Logo
            onClick={() => handleRedirect('/')}
            src={LogoRal}
            alt='Logo de RAL' />
        </NavLogo>
        <NavContact>
          <NavLink onClick={() => handleRedirect('/acerca-de-nosotros')}>{`Quienes somos >`}</NavLink>
          <NavLink onClick={() => handleRedirect('/proyectos')}>{`Proyectos >`}</NavLink>
          <Button onClick={() => { divRef.current.scrollIntoView({ behavior: 'smooth' }); }}>Contacto</Button>
        </NavContact>
      </NavbarContainer>
      <MainSection>
        <ContactTitle>
          CONSTRUCTORA RAL DE OCCIDENTE
        </ContactTitle>
        <SectionButtonContact>
          <ButtonsContact onClick={() => handleButton('processButton')} disabled={statusProcessButton}>Obras en Proceso</ButtonsContact>
          <ButtonsContact onClick={() => handleButton('edificationButton')} disabled={statusEdificationButton}>Edificacion</ButtonsContact>
          <ButtonsContact onClick={() => handleButton('infrastructureButton')} disabled={statusInfrastructureButton}>Infraestructura Urbana</ButtonsContact>
          <ButtonsContact onClick={() => handleButton('industrialButton')} disabled={statusIndustrialButton}>Obra Industrial</ButtonsContact>
        </SectionButtonContact>
      </MainSection>
      <Row>
        {data.map((item, index) => (
          <ContainerImages
            key={index}
            onShow={() => handleModal(item, true)}
            {...item}
          />
        ))}
      </Row>
      <SectionFormulario>
        <LinkButton href={`mailto:${email}?subject="Cotizacion"`} >Hagamos equipo</LinkButton>
      </SectionFormulario>
      <section id='footer' ref={divRef}>
        <Footer id='footer' color={blue} />
      </section>
      <GlobalBlur isOpen={show} customers={false} onClose={() => setShow(false)}>
        <Modal>
          <ImgBuilingComponent src={itemSelected.img} alt={itemSelected.alt} />
          <ModalContent>
            {itemSelected.descriptions?.map(d => {
              return <p>{d}</p>
            })}
          </ModalContent>
        </Modal>
      </GlobalBlur>
    </>
  );
};
