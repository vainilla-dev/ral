import styled from "styled-components";
import { blue, black } from "../../assets/styles/colors";
import { deviceDF } from "../../assets/styles/breakPoints";

export const PlayerContainer = styled.div`
  position: relative;
`;

export const ContainerTitle = styled.div`
  text-align: center;
  padding: 0 30px;
`;
export const ContainerBtnContact = styled.div`
  padding: 0 300px;
  margin: 30px;
  @media ${deviceDF.tabletL} {
    padding: 0 200px;
  }
  @media ${deviceDF.tabletM} {
    padding: 0 100px;
  }
  @media ${deviceDF.tabletS} {
    padding: 0 60px;
  }
  @media ${deviceDF.mobileL} {
    padding: 0 60px;
  }
  @media ${deviceDF.mobileM} {
    padding: 0 40px;
  }
  @media ${deviceDF.mobileS} {
    padding: 0 10px;
  }
`;

export const SectionTitle = styled.h2`
  font-size: 32px;
  font-family: "Conv_Adelle_Light_Ita";
  margin: 0 4rem 3rem;
  color: #063970;
  text-align: center;
  @media ${deviceDF.tabletM} {
    font-size: 30px;
    margin: 0 2rem 1rem;
  }
  @media ${deviceDF.mobileL} {
    font-size: 24px;
    margin: 0 1.4rem 0.6rem;
  }
  @media ${deviceDF.mobileM} {
    font-size: 22px;
  }
`;

export const VideoSectionTitle = styled.div`
  color: ${black};
  font-family: "Conv_Adelle_Bold";
  font-size: 38px;
  align-self: center;
  padding: 50px;
  @media ${deviceDF.tabletL} {
    font-size: 33px;
    padding: 50px;
  }
  @media ${deviceDF.tabletM} {
    font-size: 30px;
    padding: 50px;
  }
  @media ${deviceDF.tabletS} {
    font-size: 27px;
  }
  @media ${deviceDF.mobileL} {
    font-size: 23px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 20px;
  }
  @media ${deviceDF.mobileS} {
    font-size: 19px;
  }
`;

export const ExperienceSection = styled.div`
  width: 100%;
  padding: 60px;
  padding-bottom: 100px;
  background-color: #ebedff;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  @media ${deviceDF.laptopS} {
    width: auto;
  }
  @media ${deviceDF.tabletL} {
    width: auto;
  }
  @media ${deviceDF.tabletM} {
    width: auto;
  }
`;

export const ExperienceGrid = styled.div`
  display: inline-grid;
  grid-template-columns: auto auto;
  width: auto;
  @media ${deviceDF.tabletM} {
    display: column;
    grid-template-columns: none;
  }
  @media ${deviceDF.mobileL} {
    display: column;
    grid-template-columns: none;
  }
  @media ${deviceDF.mobileM} {
    display: column;
    grid-template-columns: none;
  }
`;

export const ItemData = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  @media ${deviceDF.laptopS} {
    width: auto;
  }
  @media ${deviceDF.tabletL} {
    width: auto;
  }
  @media ${deviceDF.tabletM} {
    width: auto;
  }
`;

export const Item = styled.div`
  padding: 20px;
  font-size: 30px;
  text-align: center;
  margin: 50px;
  width: 500px;
  padding: 10px;
  @media ${deviceDF.laptopS} {
    width: 260px;
    font-size: 22px;
  }
  @media ${deviceDF.tabletL} {
    width: 250px;
    font-size: 20px;
  }
  @media ${deviceDF.tabletM} {
    width: 350px;
    font-size: 25px;
  }
  @media ${deviceDF.mobileL} {
    width: 340px;
    font-size: 24px;
  }
  @media ${deviceDF.mobileM} {
    width: 330px;
    font-size: 21px;
  }
`;

export const ItemIcon = styled.div`
  font-size: 100px;
  font-family: "Conv_Adelle_Bold";
  align-items: center;
  color: ${blue};
  @media ${deviceDF.tabletL} {
    width: auto;
    font-size: 65px;
    align-items: center;
  }
  @media ${deviceDF.tabletM} {
    width: auto;
    font-size: 63px;
    align-items: center;
  }
  @media ${deviceDF.mobileL} {
    align-items: center;
    width: auto;
    font-size: 60px;
  }
  @media ${deviceDF.mobileM} {
    align-items: center;
    width: auto;
    font-size: 50px;
  }
`;

export const MapContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  @media ${deviceDF.mobileL} {
    padding: 30px;
    padding-top: 65px;
  }
  @media ${deviceDF.mobileM} {
    padding: 2.4rem 0.9rem;
    padding-top: 65px;
  }
`;

export const CoverageSection = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 5vh;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const TextCoverage = styled.div`
  font-size: 32px;
  font-family: "Conv_Adelle_Bold";
  align-items: center;
  color: ${blue};
`;

export const MapImg = styled.img`
  border-radius: 15px;
  margin: 5px;
  width: ${(props) => (props.IconPlace ? "100px" : "600px")};
  height: ${(props) => (props.IconPlace ? "100px" : props.height)};
  @media ${deviceDF.tabletM} {
    height: ${(props) => (props.IconPlace ? "100px" : "500px")};
  }
  @media ${deviceDF.mobileL} {
    width: ${(props) => (props.IconPlace ? "100px" : "100%")};
    height: ${(props) => (props.IconPlace ? "100px" : "auto")};
  }
  @media ${deviceDF.mobileM} {
    width: ${(props) => (props.IconPlace ? "100px" : "100%")};
    height: ${(props) => (props.IconPlace ? "100px" : "auto")};
  }
`;

export const BoxMap = styled.img`
  width: 700px;
  height: auto;
  @media ${deviceDF.tabletM} {
    width: 380px;
  }
  @media ${deviceDF.mobileL} {
    width: 360px;
  }
  @media ${deviceDF.mobileM} {
    width: 330px;
  }
`;
