import { Link } from "react-router-dom";
import styled from "styled-components";
import { black, white } from "../../assets/styles/colors";
import { deviceDF } from "../../assets/styles/breakPoints";

export const FooterContainer = styled.footer`
  width: 100%;
  height: auto;
  padding: 1.2rem 15rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  background-color: ${(props) => (props.color ? props.color : white)};
  @media ${deviceDF.laptopL} {
    padding: 1.2rem 10rem;
  }
  @media ${deviceDF.laptopS} {
    flex-direction: column;
    height: auto;
    padding: 1.2rem 4rem;
  }
  @media ${deviceDF.tabletM} {
    padding: 1.2rem 2rem;
  }
  @media ${deviceDF.mobileL} {
    padding: 1.2rem 0.4rem;
  }
  @media ${deviceDF.mobileM} {
    padding: 1.2rem 0.3rem;
  }
  @media ${deviceDF.mobileS} {
    padding: 1.2rem 0.2rem;
  }
`;

export const GroupContainer = styled.div`
  margin-top: 3vh;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media ${deviceDF.tabletL} {
    margin-bottom: 1.8rem;
    flex-direction: column;
    align-items: center;
  }
  @media ${deviceDF.tabletM} {
    margin-bottom: 1.8rem;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
  }
  @media ${deviceDF.mobileM} {
    margin-bottom: 1.8rem;
    flex-direction: column;
    align-items: center;
  }
`;

export const Text = styled.p`
  font-family: "Conv_Adelle_Semi_Bold";
  font-size: 21px;
  color: ${white};
  text-align: left;
  @media ${deviceDF.laptopL} {
    font-size: 19px;
  }
  @media ${deviceDF.laptopM} {
    font-size: 17px;
  }
  @media ${deviceDF.tabletL} {
    text-align: center;
    font-size: 20px;
    margin: 12px;
  }
  @media ${deviceDF.tabletS} {
    font-size: 18px;
    margin: 10px;
  }
  @media ${deviceDF.mobileL} {
    font-size: 16px;
    margin: 10px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 14px;
    margin: 10px;
  }
`;

export const TextConfidence = styled.div`
  font-family: "Conv_Adelle_Bold_Ita";
  font-size: 45px;
  margin: auto 0 auto 3vw;
  width: 30%;
  height: auto;
  color: ${white};
  text-align: center;
  @media ${deviceDF.tabletL} {
    margin: 0;
    font-size: 30px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 28px;
  }
  @media ${deviceDF.mobileS} {
    width: auto;
    font-size: 28px;
  }
`;

export const TextAnchor = styled.a`
  font-size: 20px;
  color: ${(props) => (props["color-text"] ? props["color-text"] : black)};
  font-family: ${(props) => props["font-family"] || "Conv_Adelle_Bold"};
  @media ${deviceDF.mobileL} {
    text-align: center;
    font-size: 18px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 16px;
  }
`;

export const TextLink = styled(Link)`
  font-size: 20px;
  color: ${(props) => (props["color-text"] ? props["color-text"] : black)};
  font-family: "Conv_Adelle_Bold";
  @media ${deviceDF.mobileL} {
    text-align: center;
    font-size: 18px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 16px;
  }
`;

export const Logo = styled.img`
  height: 70px;
  cursor: pointer;
`;

export const ImgAnchor = styled.a`
  text-decoration: underline;
`;

export const SocialMediaGroup = styled.div`
  padding: 0 3vw;
  display: flex;
  flex-direction: column;
  width: 30%;
  height: auto;
  align-items: center;
  @media ${deviceDF.tabletL} {
    width: auto;
    padding: 3vw 0;
    text-align: center;
    font-size: 21px;
  }
  @media ${deviceDF.tabletM} {
    font-size: 20px;
    width: auto;
  }
  @media ${deviceDF.mobileL} {
    font-size: 18px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 16px;
    padding: 5px;
  }
  @media ${deviceDF.mobileS} {
    padding: 8px;
  }
`;

export const SocialMediaTitle = styled.p`
  font-family: "Conv_Adelle_Bold";
  font-size: 25px;
  text-align: flex-start;
  color: ${white};
`;

export const SocialMediaLogos = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;

export const ContainerBrand = styled.div`
  display: flex;
  flex-direction: column;
  width: 40%;
  height: auto;
  @media ${deviceDF.tabletL} {
    width: auto;
    padding: auto;
  }
  @media ${deviceDF.mobileL} {
    margin: 15px auto;
  }
  @media ${deviceDF.mobileM} {
    font-size: 16px;
  }
  @media ${deviceDF.mobileS} {
    padding: 8px;
    margin: 30px 30px;
  }
`;

export const ContainerLogo = styled.div`
  align-items: center;
  @media ${deviceDF.tabletL} {
    display: flex;
    justify-content: center;
  }
`;

export const BrandContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 30px;
  @media ${deviceDF.mobileL} {
    flex-direction: column;
  }
`;


export const TextBrand = styled.p`
  font-family: "Conv_Adelle_Semi_Bold";
  font-size: 21px;
  color: ${white};
  text-align: left;
  @media ${deviceDF.laptopL} {
    font-size: 19px;
  }
  @media ${deviceDF.laptopM} {
    font-size: 17px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 20px;
    margin: 12px;
  }
  @media ${deviceDF.tabletS} {
    font-size: 18px;
    margin: 10px;
  }
  @media ${deviceDF.mobileL} {
    text-align: center;
    font-size: 16px;
    margin: 10px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 14px;
    margin: 10px;
  }
`;

export const PrivacyPolicyContainer = styled.div`
  margin-bottom: 3vh;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const PrivacyPolicyAnchor = styled.a`
  font-family: "Conv_Adelle_Semi_Bold";
  font-size: 20px;
  color: ${white};
  text-decoration: underline;
  @media ${deviceDF.laptopL} {
    font-size: 18px;
  }
  @media ${deviceDF.laptopM} {
    font-size: 16px;
  }
  @media ${deviceDF.tabletL} {
    font-size: 18px;
    margin: 12px;
  }
  @media ${deviceDF.tabletS} {
    font-size: 16px;
  }
  @media ${deviceDF.mobileL} {
    font-size: 14px;
  }
  @media ${deviceDF.mobileM} {
    font-size: 12px;
  }
`;
