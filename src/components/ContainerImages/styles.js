import styled from "styled-components";
import { deviceDF } from "../../assets/styles/breakPoints";

export const ContainerImg = styled.div`
  width: 25%;
  margin: 0px;
  padding: 0px;
  @media ${deviceDF.laptopS} {
    width: 33%;
  }
  @media ${deviceDF.tabletM} {
    width: 70%;
  }
  @media ${deviceDF.mobileM} {
    width: 90%;
  }
`;

export const ImgBuilingComponent = styled.img`
  width: 100%;
  height: 270px;
  padding: 10px;
  border-radius: 40px;
  object-fit: cover;
  @media ${deviceDF.laptopS} {
    height: 220px;
  }
  @media ${deviceDF.tabletM} {
    height: 270px;
  }
`;
