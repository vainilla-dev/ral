import React from "react";
import { useHistory } from "react-router-dom";
import { black } from "../../assets/styles/colors";
import { BurgerIcon } from "../BurgerIcon";
import {
  MenuContainer,
  MenuList,
  MenuListItem,
  CloseContainer,
} from "./styles";


export const BurgerMenu = ({ close }) => {
  let history = useHistory();

  const handleRedirect = (path) => {
    history.push(path);
    close();
  };

  return (
    <>
      <CloseContainer>
        <BurgerIcon onClick={close} open color={black} />
      </CloseContainer>
      <MenuContainer>
        <MenuList>
          <MenuListItem onClick={() => handleRedirect("/acerca-de-nosotros")}>
            Quienes somos
          </MenuListItem>
          <MenuListItem onClick={() => handleRedirect("/proyectos")}>
            Proyectos
          </MenuListItem>
        </MenuList>
      </MenuContainer>
    </>
  );
};
