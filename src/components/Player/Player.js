import React from "react";
import ReactPlayer from "react-player";
import styled from "styled-components";

const Player = ({ className, url }) => {
  return (
    <ReactPlayer
      url={url}
      className={className}
      width="100%"
      height="100%"
      playing
      muted
      controls={false}
      loop
    />
  );
};

const AbsolutelyPositionedPlayer = styled(Player)`
  position: absolute;
  top: 0;
  left: 0;
`;

const RelativePositionWrapper = styled.div`
  position: relative;
  padding-top: 56.25%;
`;

const ResponsiveStyledPlayer = (playerProps) => (
  <RelativePositionWrapper>
    <AbsolutelyPositionedPlayer {...playerProps} />
  </RelativePositionWrapper>
);

export default ResponsiveStyledPlayer;
