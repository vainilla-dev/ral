import React from "react";
import { Button } from "../Button"
import { ClientButtonsContainer } from './styles'

export const ClientButtonsContact = ({ children, ...props }) => {
  return (
    <>
      <ClientButtonsContainer {...props}>
        <Button {...props}>{`${children}`}</Button>
      </ClientButtonsContainer>
    </>
  );
};
 