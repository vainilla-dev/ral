import styled from "styled-components";
import { deviceDF } from "../../assets/styles/breakPoints";

export const Icon = styled.img`
  margin-left: 10px;
  width: 16px;
  @media ${deviceDF.tabletM} {
    width: 22px;
  }
  @media ${deviceDF.mobileL} {
    width: 18px;
  }
`;

export const ButtonsContainer = styled.div`
  margin: 28px;
  @media ${deviceDF.laptopL} {
    width: auto;
  }
  @media ${deviceDF.tabletL} {
    margin: 10px;
  }
  @media ${deviceDF.tabletM} {
    margin: 9px;
  }
`;

export const ClientButtonsContainer = styled.div`
  margin: 28px;
  display: ${(props) => (props.smallScreen ? 'none' : 'block')};
  @media ${deviceDF.laptopL} {
    width: auto;
  }
  @media ${deviceDF.tabletL} {
    display: ${(props) => (props.smallScreen ? 'block' : 'none')};
    margin: 10px;
  }
  @media ${deviceDF.tabletM} {
    margin: 9px;
  }
`;
